#include <stdio.h>

int ipow(int base, int exp)
{
    int result = 1;
    while (exp)
    {
        if (exp & 1)
            result *= base;
        exp >>= 1;
        base *= base;
    }

    return result;
}

int main() {
    int t,i,j;
    for(;;t++) {
        for(j=1;j<=8;j++) {
            for(i=0;i<1000;i++) {
                int n;
                n = ipow(2,j);
                putchar(i*14.08*n);
            }
        }
    }
}

