main() {
    int t, i;
    for (;;t++) {
        putchar((t<<6|t<<8)>>(t>>11));
        for (i=0;i<10000;i++) {
            putchar((t*i<<2|i<<t*3)>>(i>>2));
        }
        for (i=0;i<1000;i++) {
            putchar((t*i>>2)*(i>>2));
        }
    }
}

