# bytebeat

## crowd 

[...]

The interesting thing is that Crowd, like bytebeat music in general, is a piece
of rhythmic and somewhat melodic music with no score, no instruments, and no
real oscillators. It's simply a formula that defines a waveform as a function of
time, measured here in 1/8000 of a second: 

    ((t<<1)^((t<<1)+(t>>7)&t>>12))|t>>(4-(1^7&(t>>19)))|t>>7

If you put that formula into a C program with a loop that increments t, it
generates the music linked above, as follows: 

    main(t){for(;;t++)putchar(((t<<1)^((t<<1)+(t>>7)&t>>12))|t>>(4-(1^7&(t>>19)))|t>>7);}

This generates an endless headerless 8-bit unsigned mono 8ksps file on its
standard output. If you compile it as crowd, on Linux you can hear the result by
typing ./crowd | aplay, or on older installations ./crowd > /dev/dsp, because
aplay and /dev/dsp default to 8-bit unsigned mono 8ksps. I generated the Ogg
file linked above as follows: 

    ./crowd | head -c 4M > crowd.raw
    sox -r 8000 -c 1 -t u8 crowd.raw crowd.wav
    oggenc crowd.wav

http://canonical.org/~kragen/bytebeat/

## sawtooth

[...]

A simple for(;;)putchar(t++) generates a sawtooth wave with a cycle length of
256 bytes, resulting in a frequency of 31.25 Hz when using the the default
sample rate of 8000 Hz. The pitch can be changed with multiplication. t++*2 is
an octave higher, t++*3 goes up by 7 semitones from there, t++*(t>>8) produces a
rising sound.

http://countercomplex.blogspot.fr/2011/10/algorithmic-symphonies-from-one-line-of.html

## compile and play

    gcc code.c -o code
    ./code | aplay 

or with sin, cos, etc.

    gcc -xc -lm code.c
    ./code | aplay

## notes

    for (;;t++) {
        putchar(t)
        printf("%d\n",t);
    }

will print A at 65 and A again at 321 and etc. each 256

    putchar(65);  // A
    putchar(321); // A

|DEC   | BIN            |8BIT    |
|------|----------------|--------|
|65    | 01000001       |01000001|
|321   | 101000001      |01000001|
|577   | 1001000001     |01000001|
|833   | 1101000001     |01000001|

...

8000 Hz / 256 = 31.25

t++     = 31.25 Hz
t++*2   = 62.50 Hz
t++*4   = 125
t++*8   = 250
t++*16  = 500
t++*32  = 1000


8000 / 256 = 31.25
8000 / 18.18 = 440
256 / 18.18 = 14.08

    putchar(t*14.08); //440 ?
